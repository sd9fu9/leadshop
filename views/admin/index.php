<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="icon" href="/favicon.ico">
    <script src="https://at.alicdn.com/t/font_2072967_lpkwswrk7a.js"></script>
    <link rel="stylesheet" type="text/css" href="//at.alicdn.com/t/font_2072967_xvm7xzuxrm.css">
    <title><?=$title?></title>
    <script>
    let origin = window.location.origin;
    var $_W = {
        AppURL: origin+"/index.php?q=",
        AppWEB: origin+"/index.php?r=wechat",
        AppID: "98c08c25f8136d590c",
        Copyright: "Powered By Leadshop © 2021",
        AppName: "leadmall",
        AppAlias: "fitment",
        AppSecret: "3AYpU16dZ1CY7ejqvrE39B351vanLJVD",
        AppConfig: {
            defaultRoute: "index/index",
            loginURL: "/api/leadmall/login",
            loginPage: "/login/index"
        }
    }
    </script>
    <link href="/assets/admin/css/chunk-0988b492.18621cfb.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-0c3b1da8.4942f310.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-0cc5a498.3f5904a6.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-12a84aa2.182edb64.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-200300a4.b8e31bdd.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-27c07812.18d8ff4f.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-2da85ee8.aaa87604.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-30ac35cd.a8eceb38.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-31e8d1f4.c09f62b4.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-4641a6cc.d430b095.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-4da8933e.07e2f9ce.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-4eae75cc.3fffb62e.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-4ec30682.1603172b.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-52434c1e.ddd609ee.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-5719e258.e6d0d454.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-57d03275.bf2e002c.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-5ab13d10.3c99e4fa.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-5c189756.d430b095.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-6022be36.75d0226c.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-6067b6aa.0d76ad03.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-66923e50.ab958bb0.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-7125cdb5.cc7885df.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-7565be86.333bbae3.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-7911e00a.85f2081c.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-79e8f288.b021fffb.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-7bc857eb.4338a2b8.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-7dce4c5c.92539d8d.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-9176a352.8f34701f.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-92085b4e.a1f6d9ad.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-a805c6a8.d430b095.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-a8984904.f3ccc93b.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-b08a7cac.716f0c6f.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-d2d26f44.f05aa48d.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-d746bea6.7458b866.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-d8eab670.3a16be9e.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-e5b0b03a.4474153b.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-ee2a26ea.056d339e.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-fbb46a96.f1c058ae.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-fd765414.1ae4259e.css" rel="prefetch">
    <link href="/assets/admin/css/chunk-fe6dde0c.c7ed7c26.css" rel="prefetch">
    <link href="/assets/admin/js/chunk-0988b492.0e7c2ce3.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-0c3b1da8.eda17e58.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-0cc5a498.e4a5ee96.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-12a84aa2.91e923d2.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-200300a4.5cafbcaf.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-27c07812.ecca50c6.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-2d0c774e.d90cea1f.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-2d0e24b5.034d01f9.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-2d0efcfc.11f0be36.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-2da85ee8.5d5961e8.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-30ac35cd.392f438f.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-31e8d1f4.57ab884d.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-4641a6cc.6e12f076.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-4da8933e.2bae8e13.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-4eae75cc.0b24f5a3.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-4ec30682.e9512ef8.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-52434c1e.50576180.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-5719e258.858cc75f.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-57d03275.1211475f.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-5ab13d10.0ab162d9.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-5c189756.d812eb02.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-6022be36.6f8ab28b.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-6067b6aa.574f1fbe.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-66923e50.03a54dc9.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-7125cdb5.8e0ca418.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-7565be86.6c246aca.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-7911e00a.f82fe88a.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-79e8f288.25a6e313.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-7bc857eb.4fb67870.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-7dce4c5c.f83aadd0.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-9176a352.d511cfeb.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-92085b4e.bf40ccab.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-a805c6a8.434e8dd1.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-a8984904.c7337e12.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-b08a7cac.4aea9836.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-d2d26f44.bd4165e4.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-d746bea6.419f5b74.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-d8eab670.ee13e636.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-e5b0b03a.f706474e.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-ee2a26ea.930ed54b.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-fbb46a96.bd6b6ff1.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-fd765414.2fbcc59a.js" rel="prefetch">
    <link href="/assets/admin/js/chunk-fe6dde0c.ca7f3747.js" rel="prefetch">
    <link href="/assets/admin/css/app.067ff9aa.css" rel="preload" as="style">
    <link href="/assets/admin/js/app.03599ac3.js" rel="preload" as="script">
    <link href="/assets/admin/js/chunk-vendors.c6a9a8da.js" rel="preload" as="script">
    <link href="/assets/admin/css/app.067ff9aa.css" rel="stylesheet">
</head>

<body><noscript><strong>We're sorry but finish doesn't work properly without JavaScript enabled. Please enable it to continue.</strong></noscript>
    <div id="app"></div>
    <script src="/assets/admin/js/chunk-vendors.c6a9a8da.js"></script>
    <script src="/assets/admin/js/app.03599ac3.js"></script>
</body>

</html>